﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WMS.Models;

namespace WMS.Controllers
{
    public class ParticipantController : Controller
    {
        WMSContext context = new WMSContext();

        /// <summary>
        /// get list of available participants
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            ParticipantModel participantModel = new ParticipantModel();
            participantModel.Participants = context.Participants.ToList();

            if (TempData["Message"] != null && !string.IsNullOrWhiteSpace(TempData["Message"].ToString()))
            {
                participantModel.Message = TempData["Message"].ToString();
                participantModel.MessageType = TempData["MessageType"].ToString();
            }
            return View(participantModel);
        }

        [HttpGet]
        public ActionResult Detail(int id = 0)
        {
            ParticipantModel participantModel = new ParticipantModel();
            if (id > 0)
            {
                participantModel.CurrentParticipant = context.Participants.FirstOrDefault(participant => participant.Id == id);
                return View(participantModel);
            }
            else
            {
                return RedirectToAction("Index", "Participant");
            }
        }

        [HttpGet]
        public ActionResult Delete(int id = 0)
        {
            try
            {
                var currentParticipant = context.Participants.FirstOrDefault(participant => participant.Id == id);
                context.WorkshopParticipants.RemoveRange(currentParticipant.WorkshopParticipants);
                context.Participants.Remove(currentParticipant);
                context.SaveChanges();
                TempData["Message"] = "Participant Sucessfully deleted.";
                TempData["MessageType"] = "info";
            }
            catch (Exception)
            {
                TempData["Message"] = "sometype of problems are occur when deleting participant.";
                TempData["MessageType"] = "info";
            }

            return RedirectToAction("Index", "Participant");

        }

        [HttpGet]
        public ActionResult Create(int id = 0)
        {
            ParticipantModel participantModel = new ParticipantModel();
            participantModel.AvailableWorkshops = new List<SelectListItem>();
            var workshops = context.Workshops.ToList();
            if (id > 0)
            {
                participantModel.CurrentParticipant = context.Participants.FirstOrDefault(donor => donor.Id == id);
                
                foreach (var item in workshops)
                {
                    participantModel.AvailableWorkshops.Add(new SelectListItem()
                    {
                        Text = item.Name,
                        Value = item.Id.ToString(),
                        Selected = participantModel.CurrentParticipant.WorkshopParticipants.ToList().Exists(bd => bd.WorkshopId == item.Id)
                    });
                }
            }
            else
            {
                participantModel.CurrentParticipant = new Participant();
                participantModel.AvailableWorkshops = new List<SelectListItem>();
                workshops.ForEach(workshop => participantModel.AvailableWorkshops.Add(new SelectListItem()
                {
                    Text = workshop.Name,
                    Value = workshop.Id.ToString()
                }));
                
            }


            return View(participantModel);

        }

        [HttpPost]
        public ActionResult Create(Participant currentParticipant, int[] SelectedWorkshops)
        {
            try
            {
                if (currentParticipant.Id == 0)
                {
                    currentParticipant = context.Participants.Add(currentParticipant);
                    context.SaveChanges();
                    foreach (var item in SelectedWorkshops)
                    {
                        WorkshopParticipant workshopParticipant = new WorkshopParticipant();
                        workshopParticipant.WorkshopId = item;
                        workshopParticipant.ParticipantId = currentParticipant.Id;                       
                        context.WorkshopParticipants.Add(workshopParticipant);
                        context.SaveChanges();
                    }

                    TempData["Message"] = "Participant is Saved Sucessfully.";
                    TempData["MessageType"] = "saved";
                }
                else
                {

                    var selectedParticipant = context.Participants.FirstOrDefault(hospital => hospital.Id == currentParticipant.Id);
                    selectedParticipant.Name = currentParticipant.Name;
                    context.SaveChanges();

                    context.WorkshopParticipants.RemoveRange(selectedParticipant.WorkshopParticipants);
                    context.SaveChanges();

                    foreach (var item in SelectedWorkshops)
                    {
                        WorkshopParticipant workshopParticipant = new WorkshopParticipant();
                        workshopParticipant.WorkshopId = item;
                        workshopParticipant.ParticipantId = currentParticipant.Id;
                        context.WorkshopParticipants.Add(workshopParticipant);
                        context.SaveChanges();
                    }


                    TempData["Message"] = "Participant is Saved Sucessfully.";
                    TempData["MessageType"] = "info";
                }

            }
            catch (Exception ex)
            {
                TempData["Message"] = "Sometype of problems are occur when saving Participant.";
                TempData["MessageType"] = "danger";
            }


            return RedirectToAction("Index", "Participant");

        }
    }
}
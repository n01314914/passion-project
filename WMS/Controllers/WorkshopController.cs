﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WMS.Models;

namespace WMS.Controllers
{
    public class WorkshopController : Controller
    {
        WMSContext context = new WMSContext();

        public ActionResult Index()
        {
            WorkshopModel workshopModel = new WorkshopModel();
            workshopModel.Workshops = context.Workshops.ToList();

            if (TempData["Message"] != null && !string.IsNullOrWhiteSpace(TempData["Message"].ToString()))
            {
                workshopModel.Message = TempData["Message"].ToString();
                workshopModel.MessageType = TempData["MessageType"].ToString();
            }
            return View(workshopModel);
        }

        public ActionResult Create(int id = 0)
        {
            WorkshopModel workshopModel = new WorkshopModel();
            if (id > 0)
            {
                workshopModel.CurrentWorkshop = context.Workshops.FirstOrDefault(work => work.Id == id);
            }
            else
            {
                workshopModel.CurrentWorkshop = new Workshop();
            }
            return View(workshopModel);
        }

        [HttpGet]
        public ActionResult Detail(int id = 0)
        {
            WorkshopModel workshopModel = new WorkshopModel();
            if (id > 0)
            {
                workshopModel.CurrentWorkshop = context.Workshops.FirstOrDefault(donor => donor.Id == id);
                return View(workshopModel);
            }
            else
            {
                return RedirectToAction("Index", "Workshop");
            }
        }

        [HttpGet]
        public ActionResult Delete(int id = 0)
        {
            try
            {
                var currentWorkshop = context.Workshops.FirstOrDefault(workshop => workshop.Id == id);
                context.WorkshopParticipants.RemoveRange(currentWorkshop.WorkshopParticipants);
                context.Workshops.Remove(currentWorkshop);
                context.SaveChanges();
                TempData["Message"] = "Workshop id Sucessfully deleted.";
                TempData["MessageType"] = "info";
            }
            catch (Exception)
            {
                TempData["Message"] = "Some type of problems are occur when deleting a workshop.";
                TempData["MessageType"] = "info";
            }

            return RedirectToAction("Index", "Workshop");

        }

        [HttpPost]
        public ActionResult Create(Workshop currentWorkshop)
        {
            try
            {
                if (currentWorkshop.Id == 0)
                {

                    currentWorkshop = context.Workshops.Add(currentWorkshop);
                    context.SaveChanges();

                    TempData["Message"] = "Workshop is Saved Sucessfully.";
                    TempData["MessageType"] = "saved";
                }
                else
                {

                    var selectedDonor = context.Workshops.FirstOrDefault(workshop => workshop.Id == currentWorkshop.Id);
                    selectedDonor.Name = currentWorkshop.Name;
                    selectedDonor.Type = currentWorkshop.Type;
                    selectedDonor.Date = currentWorkshop.Date;
                    context.SaveChanges();


                    TempData["Message"] = "Workshop is Saved Sucessfully.";
                    TempData["MessageType"] = "info";
                }

            }
            catch (Exception ex)
            {
                TempData["Message"] = "Some type of problems are occur when saving workshop.";
                TempData["MessageType"] = "unexcepted";
            }


            return RedirectToAction("Index", "Workshop");

        }

    }
}
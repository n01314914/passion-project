﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WMS.Models
{
    public class ParticipantModel : BaseModel
    {
        public List<Participant> Participants { get; set; }

        public Participant CurrentParticipant { get; set; }

        public List<SelectListItem> AvailableWorkshops { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Please select workshop.")]
        public int[] SelectedWorkshops { get; set; }
    }
}
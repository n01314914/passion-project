﻿namespace WMS
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    
    public partial class WMSContext : DbContext
    {
        public WMSContext()
            : base("name=Workshop_Context")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            
        }
    
        public virtual DbSet<Participant> Participants { get; set; }
        public virtual DbSet<WorkshopParticipant> WorkshopParticipants { get; set; }
        public virtual DbSet<Workshop> Workshops { get; set; }
    }
}

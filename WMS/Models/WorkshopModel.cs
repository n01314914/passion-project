﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WMS.Models
{
    public class WorkshopModel : BaseModel
    {
        public List<Workshop> Workshops { get; set; }

        public Workshop CurrentWorkshop { get; set; }
    }
}